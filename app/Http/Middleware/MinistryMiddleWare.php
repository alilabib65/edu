<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class MinistryMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next ,$guard = null)
    {

                if(Auth::user()->level =='ministry' ) {
                    return redirect(localeUrl(route('admin-panel')));
                }else if(Auth::user("admin")->level =='area') {
                    return redirect(localeUrl(route('area')));
                }




    }
}
