<?php

function reservation_detail() { return new App\Models\ReservationDetail;}
function category() { return new App\Models\Category;}
function hall() { return new App\Models\Hall;}
function provider() { return new App\Models\Provider;}
function product() { return new App\Models\Product;}
function reservation() { return new App\Models\Reservation;}
function user() { return new App\Models\User;}
function admin() { return new App\Models\Admin;}
function city() { return new App\Models\City;}
//function setting() { return new App\Models\Setting;}
function service() { return new App\Models\Service;}
function slider() { return new App\Models\Slider;}
function str() { return new \Str;}
function direction() { return app()->isLocale('ar') ? 'rtl' : 'ltr'; }
function floating($right, $left) { return app()->isLocale('ar') ? $right : $left; }

function slider_translation() { return new App\Models\SliderTranslation;}
function product_translation() { return new App\Models\ProductTranslation;}
function category_translation() { return new App\Models\CategoryTranslation;}
function city_translation() { return new App\Models\CityTranslation;}
function hall_translation() { return new App\Models\HallTranslation;}
function service_translation() { return new App\Models\ServiceTranslation;}

function localeUrl($url, $locale = null) { return LaravelLocalization::getLocalizedURL($locale, $url); }

function categories() { return category()->where('status', 1)->get(); }
function cities() { return city()->where('status', 1)->get(); }
function getSetting($setting_name) { return setting()->where('key', $setting_name)->first()->value; }

function getImage($type, $img)
{
	if($type == 'user')
		return ($img != null) ? asset('uploaded/users/'.$img) : asset('admin/img/avatar10.jpg');

	elseif($type == 'admins')
		return ($img != null) ? asset('uploaded/admins/'.$img) : asset('admin/img/avatar10.jpg');

	elseif($type == 'providers')
		return ($img != null) ? asset('uploaded/providers/'.$img) : asset('front/img/icons/avatar1.png');

	elseif($type == 'halls')
		return ($img != null) ? asset('uploaded/halls/'.$img) : asset('admin/assets/images/hall.png');

	elseif($type == 'categories')
		return ($img != null) ? asset('uploaded/categories/'.$img) : asset('admin/assets/images/placeholder.jpg');

	elseif($type == 'services')
		return ($img != null) ? asset('uploaded/services/'.$img) : asset('admin/assets/images/placeholder.jpg');

	elseif($type == 'sliders')
		return ($img != null) ? asset('uploaded/sliders/'.$img) : asset('admin/assets/images/placeholder.jpg');

	elseif($type == 'products')
		return ($img != null) ? asset('uploaded/products/'.$img) : asset('admin/assets/images/placeholder.jpg');

	else
		return asset('admin/img/avatar10.jpg');
}

function models()
{
	// those models for CRUD stystem only;
	return [
		'user-tie'    => 'admin',
		'users'       => 'user',
		'stack2'      => 'category',
		'city'        => 'city',
		'home7'       => 'hall',
		'cart'        => 'product',
		'users2'      => 'provider',
		'gear'        => 'setting',
		'statistics'  => 'service',
		'screen-full' => 'slider',
	];
}

function getProductsByProviderId($provider_id, $auth)
{
	$provider = provider()->find($provider_id);

	$reservations = reservation_detail()->where('provider_id', $provider->id)->where('user_id', $auth)

	->select(DB::Raw('reservation_details.*, sum(qty) as total'))

	->groupBy('product_id')

	->get();

	return $reservations;
}

function getProviderProductsByCategoryId($category_id)
{
	$category = category()->find($category_id);

	return ($category->providers) ? $category->providers : false;
}

function getModelCount($model, $withDeleted = false)
{
	if($withDeleted)
	{
		if($model == 'admin') return admin()->onlyTrashed()->where('is_super_Admin', '!=', 1)->count();

		$mo = "App\\Models\\".ucwords($model);

		return $mo::onlyTrashed()->count();
	}

	if($model == 'admin') return admin()->where('is_super_Admin', '!=', 1)->count();

	$mo = "App\\Models\\".ucwords($model);

	return $mo::count();
}

function getProvidersByUserId($user_id, $category_id)
{
	$user = user()->find($user_id);
	$providers_ids = reservation_detail()->where('user_id', $user_id)->get()->pluck('provider_id')->toArray();
	$providers = provider()->whereIn('id', array_unique($providers_ids))->where('category_id', $category_id)->get();
	return $providers;
}

function getUserProductsByProviderId($user_id, $provider_id)
{
	$user = user()->find($user_id);
	$products_ids = reservation_detail()->where('user_id', $user_id)->get()->pluck('product_id')->toArray();
	$products = product()->whereIn('id', array_unique($products_ids))->where('provider_id', $provider_id)->get();
	return $products;
}

function save_translated_attrs($model, $formTranslatedAttrs, $otherAttrs = null, $excepted = [])
{
	foreach (config('sitelangs.locales') as $lang => $name)
	{
		foreach ($model->translatedAttributes as $attr)
		{
			$model->translateOrNew($lang)->$attr = $formTranslatedAttrs[$lang][$attr];
		}
	}

	if ($otherAttrs != null) {
		$otherAttrs = array_except($otherAttrs, ['_method','_token'] + $excepted);
		foreach ($otherAttrs as $key => $value) { $model->$key = $value; }
	}
}

function load_translated_attrs($model)
{
	foreach (config('sitelangs.locales') as $lang => $name)
	{
		$model->$lang = [];
		foreach ($model->translatedAttributes as $attr) $model->$lang = $model->$lang + [$attr => $model->getTranslation($lang)->$attr];
	}
}


function uploadImg($request,$url,$model=null,$field='image',$width=400,$height=400)
{
  $dist='images'.DIRECTORY_SEPARATOR.$url.DIRECTORY_SEPARATOR;
  $image=$dist.'photo.jpg';
  if ($request->hasFile($field)) {
    if ($model && $model->$field != $dist.'photo.jpg') {
      File::delete($model->$field);
    }
    if (gettype($request->file($field)) == 'array') {
      $image=[];
        foreach ($request->file($field) as $img) {
          if ($img) {
            Image::make($img)->resize($width,$height,null,function($cons){
              $cons->aspectRatio();
            })->save($dist.$img->hashName());
            $image[]=$dist.$img->hashName();
          }
        }
    }else{
      Image::make($request->$field)->resize($width,$height,function($cons){
        $cons->aspectRatio();
      })->save($dist.$request->$field->hashName());
      $image=$dist.$request->$field->hashName();
    }
  }
  return $image;
}
