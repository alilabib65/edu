<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Admin;

class EditAdminRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $admin  = Admin::find($this->admin);

        $rules = [
            'name'  => 'required|string',
            'email' => 'required|email|unique:admins,email,'.$admin->id,
            'image' => 'nullable|image|mimes:jpg,png,jpeg|max:1000',
        ];

        return $rules;
    }
}
