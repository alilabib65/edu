<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class EditStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    =>'required',
            'email'   =>'nullable|email|unique:students,email',
            'image'   =>'nullable|image|mimes:jpg,png,jpeg',
            'dob'     =>'required',
            'level_id'=>'required',
            'gender'  =>'required',
            'father_salary'=>'required',
            'father_phone'=>'required',
            'father_job'=>'string',
        ];
    }
}
