<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class SendMailRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules =  [
            'to'      => 'required|email',
            'subject' => 'required|string',
            'message' => 'required|string',
        ];

        return $rules;
    }
}
