<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateSettingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'key'   => 'required|string|unique:settings,key',
            'value' => 'required|string',
            'image' => 'nullable|image|mimes:jpg,png,jpeg|max:1000',
        ];

        return $rules;
    }
}
