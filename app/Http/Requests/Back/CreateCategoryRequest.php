<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules =  [
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048'
        ];
        
        foreach (config('sitelangs.locales') as $lang => $name) {
            $rules[$lang.'.name']    = 'required|string';
        }
        return $rules;
    }
}
