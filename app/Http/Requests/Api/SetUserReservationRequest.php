<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class SetUserReservationRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'userData'                                     => 'required',
            'details'                                      => 'required|array',

            'userData.name'                                => 'required|string',
            'userData.email'                               => 'required|email|unique:users,email',
            'userData.phone'                               => 'required|string',
            'userData.neighborhood'                        => 'nullable|string',
            'userData.street'                              => 'nullable|string',
            'userData.bride_name'                          => 'required|string|mak_words',

            'details.*.category.id'                        => 'required|exists:categories,id',
            'details.*.category.name'                      => 'required|string',
            'details.*.category.provider.id'               => 'required|exists:providers,id',
            'details.*.category.provider.shop_name'        => 'required|string',
            'details.*.category.provider.shop_name'        => 'required|string',

            'details.*.category.provider.products.*.id'    => 'required|exists:products,id',
            'details.*.category.provider.products.*.name'  => 'required|string',
            'details.*.category.provider.products.*.price' => 'required|numeric',
            'details.*.category.provider.products.*.count' => 'required|numeric',
            'details.*.category.provider.products.*.total' => 'required|numeric',

            'reservation_date'                             => 'required|date_format:d/m/Y',
            'hall_id'                                      => 'required|exists:halls,id',
            'finalTotal'                                   => 'required|numeric',
        ];
        return $rules;
    }
}
