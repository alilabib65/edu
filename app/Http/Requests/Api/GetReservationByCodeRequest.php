<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class GetReservationByCodeRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'code' => 'required|exists:reservations,code',
        ];
        return $rules;
    }
}
