<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class GetProductsByProviderIdRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'provider_id' => 'required|exists:providers,id',
        ];
        return $rules;
    }
}
