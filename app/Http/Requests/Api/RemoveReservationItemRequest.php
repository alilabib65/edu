<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class RemoveReservationItemRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'user_id'     => 'required|exists:users,id',
            'product_id'  => 'required|exists:products,id',
            'provider_id' => 'required|exists:providers,id',
        ];
        return $rules;
    }
}
