<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class GetHallByIdRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'hall_id' => 'required|exists:halls,id',
        ];
        return $rules;
    }
}
