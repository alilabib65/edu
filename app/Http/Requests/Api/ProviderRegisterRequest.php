<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class ProviderRegisterRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'name'         => 'required|string',
            'email'        => 'required|email|unique:providers,email',
            'phone'        => 'required|string|unique:providers,phone',
            'shop_name'    => 'required|string',
            'shop_address' => 'required|string',
            'category_id'  => 'required|exists:categories,id',
            'crn'          => 'required|string',
            'password'     => 'required|string',
        ];
        return $rules;
    }
}
