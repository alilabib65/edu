<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class GetHallsByCityIdRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'city_id' => 'required|exists:cities,id',
        ];
        return $rules;
    }
}
