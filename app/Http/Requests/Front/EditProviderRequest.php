<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Provider;

class EditProviderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $provider = Provider::find(auth()->user()->id);
        $rules = [
            'name'                        => 'required|string',
            'email'                       => 'required|email|unique:providers,email,'.$provider->id,
            'phone'                       => 'required|numeric',
            'shop_name'                   => 'required|string',
            'shop_address'                => 'required|string',
            'password'                    => 'nullable|string',
            'image'                       => 'nullable|image|mimes:jpg,png,jpeg|max:1000',
        ];

        return $rules;
    }
}
