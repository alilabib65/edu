<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class CreateProviderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'name'                        => 'required|string',
            'email'                       => 'required|email|unique:providers,email',
            'phone'                       => 'required|numeric|digits_between:11,11',
            'commercial_registration_no'  => 'required|numeric',
            'category_id'                 => 'required|numeric|exists:categories,id',
            'shop_name'                   => 'required|string',
            'shop_address'                => 'required|string',
            'password'                    => 'required',
            'image'                       => 'nullable|image|mimes:jpg,png,jpeg|max:1000',
        ];

        return $rules;
    }
}
