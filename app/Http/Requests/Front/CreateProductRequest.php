<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules =  [
            'ammount' => 'required|numeric',
            'price'   => 'required|numeric',
            'image'   => 'nullable|image|mimes:jpeg,png,jpg|max:2048'
        ];
        
        foreach (config('sitelangs.locales') as $lang => $name) {
            $rules[$lang.'.name'] = 'required|string';
        }
        return $rules;
    }
}
