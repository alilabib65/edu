<?php

namespace App\Http\Controllers\Back;

use App\Models\Employee;
use App\Models\Payment;
use App\Models\PaymentCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Back\CreatePaymentRequest;
use App\Http\Requests\Back\EditPaymentRequest;
class PaymentController extends Controller
{
    public function index()
    {
        $model = Payment::all() ;
        return view('Back.payment.index',compact('model'));
    }

    public function create()
    {
        $employees = Employee::all()->pluck('name','id');
        $categories = PaymentCategory::listsTranslations('name')->get()->pluck('name','id');
        return view('Back.payment.create',compact('categories','employees'));
    }

    public function store(CreatePaymentRequest $request)
    {
        //dd($request->all());
        $inputs=$request->except(['image','show']);
        $inputs['image']=uploadImg($request,'payment');
        $inputs['date']=Carbon::now()->format('y-m-d');
        Payment::create($inputs);
        return redirect()->route('payment')->withTrue(trans("تم الإضافة بنجاح"));
    }

    public function edit(Payment $model)
    {
        $employees = Employee::all()->pluck('name','id');
        $categories = PaymentCategory::listsTranslations('name')->get()->pluck('name','id');
        return view('Back.payment.edit',compact('model','categories','employees'));
    }

    public function update(EditPaymentRequest $request,Payment $model)
    {

        $inputs=$request->except(['image','show']);
        if ($request->image) {
            $inputs['image']=uploadImg($request,'payment',$model);
        }
        $model->update($inputs);
        return redirect()->route('payment')->withTrue("تم التعديل بنجاح");

    }

    public function delete(Payment $model)
    {
        File::delete($model->image);
        $model->delete();
        return redirect()->back();
    }
}
