<?php

namespace App\Http\Controllers\Back;

use App\Models\Level;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Income;
class IncomeController extends Controller
{
    public function index()
    {
        $model = Income::all() ;
        return view('Back.income.index',compact('model'));
    }

    public function create()
    {
        $students = Student::all()->pluck('name','id');
        $levels = Level::listsTranslations('name')->get()->pluck('name','id');
        return view('Back.income.create',compact('students','levels'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $inputs=$request->except(['image','show']);
        $inputs['image']=uploadImg($request,'employee');
        Income::create($inputs);
        return redirect()->route('income')->withTrue(trans("تم الإضافة بنجاح"));
    }

    public function edit(Income $model)
    {
        $students = Student::all();
        $levels = Level::listsTranslations('name')->get()->pluck('name','id');
        return view('Back.income.edit',compact('model','students','levels'));
    }

    public function update(Request $request,Income $model)
    {

        $inputs=$request->except(['image','show']);
        if ($request->image) {
            $inputs['image']=uploadImg($request,'income',$model);
        }
        $model->update($inputs);
        return redirect()->route('income')->withTrue("تم التعديل بنجاح");

    }

    public function delete(Income $model)
    {
        File::delete($model->image);
        $model->delete();
        return redirect()->back();
    }
}
