<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Level;
use File;
use App\Http\Requests\Back\CreateLevelRequest;
use App\Http\Requests\Back\EditLevelRequest;

class LevelController extends Controller
{
       //
       public function index()
       {

           $model = Level::all() ;
          return view('Back.Levels.index',compact('model'));
       }

       public function create()
       {
          return view('Back.Levels.create');
       }

       public function store(CreateLevelRequest $request)
       {
           //dd($request->all());

           $inputs=$request->except(['image','show']);
           $inputs['image']=uploadImg($request,'level');
           Level::create($inputs);
           return redirect()->route('level')->withTrue(trans("تم الإضافة بنجاح"));
       }

       public function edit(Level $model)
       {
          return view('Back.Levels.edit',compact('model'));
       }

       public function update(EditLevelRequest $request,Level $model)
       {

           $inputs=$request->except(['image','show']);
           if ($request->image) {
                   $inputs['image']=uploadImg($request,'level',$model);
           }
           $model->update($inputs);
           return redirect()->route('level')->withTrue("تم التعديل بنجاح");

       }

       public function delete(Level $model)
       {
           File::delete($model->image);
           $model->delete();
           return redirect()->back();
       }
}
