<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin;

class AdminsController extends Controller
{
    //
     public function index()
     {
        $model = Admin::all() ;
       return view('Back.Admins.index',compact('model'));
     }

     public function create()
     {
        return view('Back.Admins.create');
     }

     public function store(Request $request)
     {
        $inputs=$request->except(['image','show']);
        $inputs['image']=uploadImg($request,'admin');
        Admin::create($inputs);
        return redirect()->route('service')->withTrue(trans("تم الإضافة بنجاح"));
     }

     public function edit(Admin $model)
     {
        return view('Back.Admins.edit',compact('model'));
     }

     public function update(Request $request, Admin $model)
     {
        $inputs=$request->except(['image','show']);
        if ($request->image) {
                $inputs['image']=uploadImg($request,'service',$model);
        }
        $model->update($inputs);

        return redirect()->route('service')->withTrue("تم التعديل بنجاح");
     }

     public function delete(Admin $model)
     {
        File::delete($model->image);
        $model->delete();
        return redirect()->back();
     }
}
