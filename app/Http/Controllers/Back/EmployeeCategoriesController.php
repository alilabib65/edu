<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EmployeeCategory;
use File;
class EmployeeCategoriesController extends Controller
{
    //
    public function index()
    {
        $model = EmployeeCategory::all() ;
        return view('Back.employeecategory.index',compact('model'));
    }

    public function create()
    {
        return view('Back.employeecategory.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());

        $inputs=$request->except(['image','show']);
        $inputs['image']=uploadImg($request,'employeecategory');
        EmployeeCategory::create($inputs);
        return redirect()->route('employeecategory')->withTrue(trans("تم الإضافة بنجاح"));
    }

    public function edit(EmployeeCategory $model)
    {
        return view('Back.employeecategory.edit',compact('model'));
    }

    public function update(Request $request,EmployeeCategory $model)
    {

        $inputs=$request->except(['image','show']);
        if ($request->image) {
            $inputs['image']=uploadImg($request,'employeecategory',$model);
        }
        $model->update($inputs);
        return redirect()->route('employeecategory')->withTrue("تم التعديل بنجاح");

    }

    public function delete(Employee $model)
    {
        File::delete($model->image);
        $model->delete();
        return redirect()->back();
    }
}
