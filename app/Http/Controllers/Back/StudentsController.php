<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentLevelPayment;
use App\Models\Level;
use File;
use App\Http\Requests\Back\CreateStudentRequest;
use App\Http\Requests\Back\EditStudentRequest;
class StudentsController extends Controller
{
       //
       public function index()
       {
           $model = Student::all() ;
          return view('Back.Students.index',compact('model'));
       }

       public function create()
       {
           $levels = Level::listsTranslations('name')->get()->pluck('name','id');
          return view('Back.Students.create',compact('levels'));
       }

       public function store(CreateStudentRequest $request)
       {
           //dd($request->all());
           $level = Level::find($request->level_id);
           //dd($level);

           $inputs=$request->except(['image','show','type']);
           $inputs['image']=uploadImg($request,'student');
           $student =    Student::create($inputs);


           $levelPayment = new StudentLevelPayment();
           $levelPayment->student_id = $student->id;
           $levelPayment->level_id   = $request->level_id;
           $levelPayment->type       = ($request->type  !=0 ) ? "bus":"no_bus";
           $levelPayment->total      = ($request->type  !=0 ) ? $level->bus_price :$level->no_bus_price;
           $levelPayment->active     = 1;
           $levelPayment->save();

           return redirect()->route('student')->withTrue(trans("تم الإضافة بنجاح"));
       }

       public function edit(Student $model)
       {
           $levels = Level::listsTranslations('name')->get()->pluck('name','id');
          return view('Back.Students.edit',compact('model','levels'));
       }

       public function update(EditStudentRequest $request,Student $model)
       {

           $inputs=$request->except(['image','show','type']);
           if ($request->image) {
                   $inputs['image']=uploadImg($request,'student',$model);
           }
           $model->update($inputs);

           $studentLevelPaymentCount = StudentLevelPayment::where('student_id',$model->id)->where('level_id',$request->level_id)->count();

               if($studentLevelPaymentCount == 0 ){
                   $level = Level::find($request->level_id);
                   $levelPayment = new StudentLevelPayment();
                   $levelPayment->student_id = $model->id;
                   $levelPayment->level_id   = $request->level_id;
                   $levelPayment->type       = ($request->type  !=0  ) ? "bus":"no_bus";
                   $levelPayment->total      = ($request->type  != 0 ) ? $level->bus_price : $level->no_bus_price;
                   $levelPayment->active     = 1;
                   $levelPayment->save();
               }

           return redirect()->route('student')->withTrue("تم التعديل بنجاح");

       }

       public function delete(Student $model)
       {
           File::delete($model->image);
           $model->delete();
           return redirect()->back();
       }
}
