<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    //
    public function index()
    {
        return view('Back.includes.index');
    }

    public function create()
    {
       return view('');
    }

    public function store(Request $request)
    {

       return redirect()->back()->with(['message'=>'Data saved successfully']);
    }

    public function edit($model)
    {
       return view('',compact('model'));
    }

    public function update(Request $request, $model)
    {

       return redirect()->back()->with(['message'=>'Data saved successfully']);
    }

    public function delete($model)
    {
        $model->delete();

    }
}
