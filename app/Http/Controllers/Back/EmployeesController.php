<?php

namespace App\Http\Controllers\Back;

use App\Models\EmployeeCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use File;
class EmployeesController extends Controller
{
       //
       public function index()
       {
           $model = Employee::all() ;
          return view('Back.Employee.index',compact('model'));
       }

       public function create()
       {
           $parentModel = EmployeeCategory::listsTranslations('name')->get()->pluck('name','id');
          return view('Back.Employee.create',compact('parentModel'));
       }

       public function store(Request $request)
       {
           //dd($request->all());

           $inputs=$request->except(['image','show','identity_image','_token']);
           $inputs['image']=uploadImg($request,'employee');
           $inputs['identity_image'] = uploadImg($request,'employee');
           Employee::create($inputs);
           return redirect()->route('employee')->withTrue(trans("تم الإضافة بنجاح"));
       }

       public function edit(Employee $model)
       {
           $parentModel = EmployeeCategory::listsTranslations('name')->get()->pluck('name','id');
          return view('Back.Employee.edit',compact('model','parentModel'));
       }

       public function update(Request $request,Employee $model)
       {

           $inputs=$request->except(['image','show','identity_image','_token']);
           if ($request->image) {
                   $inputs['image']=uploadImg($request,'employee',$model);
           }

           if ($request->identity_image) {
               $inputs['identity_image'] = uploadImg($request, 'employee',$model);
           }

           $model->update($inputs);
           return redirect()->route('employee')->withTrue("تم التعديل بنجاح");

       }

       public function delete(Employee $model)
       {
           File::delete($model->image);
           $model->delete();
           return redirect()->back();
       }
}
