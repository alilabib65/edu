<?php

namespace App\Http\Controllers\Back;

use App\Models\PaymentCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentCategoryController extends Controller
{
    public function index()
    {
        $model = PaymentCategory::all() ;
        return view('Back.paymentcategory.index',compact('model'));
    }

    public function create()
    {
        return view('Back.paymentcategory.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());

        $inputs=$request->except(['image','show']);
        $inputs['image']=uploadImg($request,'paymentcategory');
        PaymentCategory::create($inputs);
        return redirect()->route('paymentcategory')->withTrue(trans("تم الإضافة بنجاح"));
    }

    public function edit(PaymentCategory $model)
    {
        return view('Back.paymentcategory.edit',compact('model'));
    }

    public function update(Request $request,PaymentCategory $model)
    {

        $inputs=$request->except(['image','show']);
        if ($request->image) {
            $inputs['image']=uploadImg($request,'paymentcategory',$model);
        }
        $model->update($inputs);
        return redirect()->route('paymentcategory')->withTrue("تم التعديل بنجاح");

    }

    public function delete(PaymentCategory $model)
    {
        File::delete($model->image);
        $model->delete();
        return redirect()->back();
    }
}
