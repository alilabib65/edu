<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Schema, View;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Schema::defaultStringLength(191);

        validator()->extend('mak_words', function($attribute, $value, $parameters, $validator)
        {
            $words = explode(' ', $value);

            $nbWords = count($words);

            if($nbWords == 4) { return $value; }
        });

        validator()->extend('lang_lat', function($attribute, $value) {
            return preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $value);
        });

        validator()->extend('youtube_link', function($attribute, $value){
            return preg_match('@^(?:https://(?:www\\.)?youtube.com/)(watch\\?v=|v/)([a-zA-Z0-9_]*)@', $value, $match);
        });

        validator()->extend('is_ar', function($attribute, $value){
            return preg_match('/[اأإء-ي]/ui', $value, $match);
        });

    }

    public function register()
    {
        View::composer('*', function($view){
            return $view->with('auth', auth()->user());
        });

        View::composer('*',function($view){
            $view->with('locale',app()->getLocale());
        });
    }
}
