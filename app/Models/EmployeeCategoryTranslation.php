<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeCategoryTranslation extends Model
{
    protected $table = 'employee_category_translations';
    public $timestamps = false;
    protected $fillable = ['name','desc'];
}
