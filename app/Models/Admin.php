<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash, DB, Image, Auth, File;

class Admin extends Authenticatable
{
    use Notifiable, SoftDeletes;

    public $guard = 'admin';

    protected $guarded = ['id', 'password_confirmation'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = ['status' => 'boolean'];

    public function halls()
    {
        return $this->hasMany('App\Models\Hall');
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public static function uploadImage($img)
    {
        $filename = 'admin_' . str_random(12) . '_' . date('Y-m-d') . '.' . strtolower($img->getClientOriginalExtension());

        if (!file_exists(public_path('uploaded/admins/')))
            mkdir(public_path('uploaded/admins/'), 0777, true);

        $path = public_path('uploaded/admins/');

        $img = Image::make($img)->save($path . $filename);

        return $filename;
    }

    public static function createAdmin($adminData)
    {
        if (request()->hasFile('image')) $adminData['image'] = Admin::uploadImage($adminData['image']);

        else $adminData = array_except($adminData, ['image']);

        $createdAdmin = Admin::updateOrcreate(array_except($adminData, ['_token', 'password_confirmation']));

        return $createdAdmin;
    }

    public static function updateAdmin($adminData, $currentAdmin)
    {
        if (request()->hasFile('image'))
        {
            File::delete('uploaded/admins/'.$currentAdmin->image);
            $adminData['image'] = Admin::uploadImage($adminData['image']);
        }

        else $adminData = array_except($adminData, ['image']);

        $updateAdmin = $currentAdmin->update($adminData);

        return $updateAdmin;
    }

    //
}
