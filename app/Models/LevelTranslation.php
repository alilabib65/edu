<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LevelTranslation extends Model
{
    protected $table = 'level_translations';
    public $timestamps = false;
    protected $fillable = ['name','desc'];
}
