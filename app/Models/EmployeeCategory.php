<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Dimsav\Translatable\Translatable;

class EmployeeCategory extends Model
{
    use  Translatable;

    protected $guarded = [];
    public $translatedAttributes = ['name','desc'];
}
