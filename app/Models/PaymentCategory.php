<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class PaymentCategory extends Model
{
    use  Translatable;

    protected $guarded = [];
    public $translatedAttributes = ['name','desc'];

}
