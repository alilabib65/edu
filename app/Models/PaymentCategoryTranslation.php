<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentCategoryTranslation extends Model
{
    protected $table = 'payment_category_translations';
    public $timestamps = false;
    protected $fillable = ['name','desc'];
}
