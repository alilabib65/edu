<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->enum('active',[0,1])->default(1);
            $table->timestamps();
        });

        Schema::create('employee_category_translations',function (Blueprint $table){
            $table->increments('id');
            $table->integer('employee_category_id')->unsigned();
            $table->string('name');
            $table->text('desc')->nullable();
            $table->string('locale')->index();
            $table->unique(['category_id','locale']);
            $table->foreign('category_id')->references('id')->on('employee_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
        Schema::dropIfExists('employee_categories');
    }
}
