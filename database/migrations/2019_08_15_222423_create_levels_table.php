<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('bus_price')->default('0');
            $table->string('no_bus_price')->default('0');
            $table->enum('active',[0,1])->default(1);
            $table->timestamps();
        });


        Schema::create('level_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('level_id')->unsigned();
            $table->string('name');
            $table->text('desc')->nullable();
            $table->string('locale')->index();
            $table->unique(['level_id','locale']);
            $table->foreign('level_id')->references('id')->on('levels')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('level_translations');
        Schema::dropIfExists('levels');
    }
}
