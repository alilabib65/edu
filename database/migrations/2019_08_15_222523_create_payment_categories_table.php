<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->enum('active',[0,1])->default(1);
            $table->timestamps();
        });

        Schema::create('payment_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_category_id')->unsigned();
            $table->string('name');
            $table->text('desc')->nullable();
            $table->string('locale')->index();
            $table->unique(['payment_category_id','locale']);
            $table->foreign('payment_category_id')->references('id')->on('payment_categories')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_category_translations');
        Schema::dropIfExists('payment_categories');
    }
}
