<fieldset class="content-group">

    <div class="form-group">
        <label class="control-label col-lg-2">الإسم بالعربي</label>
        <div class="col-lg-10">
            {!! Form::text("name", isset($model) ? $model->name:null , ['class'=>'form-control','placeholder'=>'الإسم']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2">البريد الإلكتروني </label>
        <div class="col-md-10">
            {!! Form::email("email", isset($model) ? $model->email:null  , ['class'=>'form-control','placeholder'=>" البريد الإلكتروني"]) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2"> الهاتف </label>
        <div class="col-lg-10">
            {!! Form::text("phone", isset($model) ? $model->phone:null , ['class'=>'form-control','placeholder'=>"الهاتف "]) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2">النوع</label>
        <div class="col-md-10">
            {!! Form::select('gender',["male"=>"ذكر","female"=>"آنثي"], null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>"إختر النوع  "]) !!}
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-lg-2">تاريخ الميلاد  </label>
        <div class="col-md-10">
            {!! Form::text("dop", isset($model) ? $model->dop:null  , ['class'=>'form-control datepicker','placeholder'=>"تاريخ الميلاد  "]) !!}
        </div>
    </div>



    <div class="form-group">
        <label class="control-label col-lg-2">العنوان  </label>
        <div class="col-md-10">
            {!! Form::text("address", isset($model) ? $model->address:null  , ['class'=>'form-control','placeholder'=>" العنوان "]) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2">المرحلة </label>
        <div class="col-md-10">
            {!! Form::select('level_id',$levels, null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>"إختر المرحلة "]) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2">الصورة</label>
        <div class="col-md-9">
            <input type="file" name="image" class="file-styled-primary image">
        </div>
        <div class="col-md-1">
            @if (isset($model->image) && $model->image != null)
                <img src="{{ asset($model->image) }}" class="img-thumbnail image-preview" style="width: 100%; height: 100%;">
            @else
                <img src="{{ asset('assets/images/placeholder_image.png') }}" class="img-thumbnail image-preview" style="width: 100%; height: 100%;">
            @endif
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2">مرتب الآمر </label>
        <div class="col-md-10">
            {!! Form::text("father_salary", isset($model) ? $model->father_salary:null  , ['class'=>'form-control','placeholder'=>"مرتب الآب "]) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2">رقم هاتف ولي الآمر </label>
        <div class="col-md-10">
            {!! Form::text("father_phone", isset($model) ? $model->father_phone:null  , ['class'=>'form-control','placeholder'=>" رقم هاتف ولي الآمر "]) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2">وظيفة ولي الآمر </label>
        <div class="col-md-10">
            {!! Form::text("father_job", isset($model) ? $model->father_job:null  , ['class'=>'form-control','placeholder'=>"  وظيفة ولي الآمر "]) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2">المصاريف نوع </label>
        <div class="col-md-10">
            {!! Form::select('type',[1=>"مصاريف الدراسة و التنقل",0=>"مصاريف دراسية فقط"], null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>" المصاريف نوع  "]) !!}
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-lg-2">تفعيل</label>
        <div class="col-md-10">
            {!! Form::select('active',[1=>"مفعل",0=>"غير مفعل"], null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>"إختر حالة التفعيل "]) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary pull-right">{{ $btnSub }}
                <i class="{{ app()->getLocale() == 'ar' ? 'icon-arrow-left13' : 'icon-arrow-right13'}} position-right"></i></button>
        </div>
    </div>
</fieldset>


@php
        $parent['route']=route('student');
        $parent['name']="الطلاب";
      @endphp

    @push('parent')
    <li><a href="{{ $parent['route'] }}"><i class="icon-price-tags position-left"></i> {{ $parent['name'] }}</a></li>
    @endpush
    @section('current',$current)
