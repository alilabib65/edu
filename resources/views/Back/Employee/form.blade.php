<fieldset class="content-group">

        <div class="form-group">
            <label class="control-label col-lg-2">الإسم بالعربي</label>
            <div class="col-lg-10">
                {!! Form::text("name", isset($model) ? $model->name:null , ['class'=>'form-control','placeholder'=>'الإسم']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-2">البريد الإلكتروني </label>
            <div class="col-md-10">
                {!! Form::email("email", isset($model) ? $model->email:null  , ['class'=>'form-control','placeholder'=>" البريد الإلكتروني"]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-2"> الهاتف </label>
            <div class="col-lg-10">
                {!! Form::text("phone", isset($model) ? $model->phone:null , ['class'=>'form-control','placeholder'=>"الهاتف "]) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-2">المرتب </label>
            <div class="col-md-10">
                {!! Form::text("salary", isset($model) ? $model->salary:null  , ['class'=>'form-control','placeholder'=>"المرتب "]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-2">رقم الهوية </label>
            <div class="col-md-10">
                {!! Form::text("identity", isset($model) ? $model->identity:null  , ['class'=>'form-control','placeholder'=>"رقم الهوية "]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-2">القسم </label>
            <div class="col-md-10">
                {!! Form::select('category_id',$parentModel, null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>"إختر القسم "]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-2">الصورة</label>
            <div class="col-md-9">
                <input type="file" name="image" class="file-styled-primary image">
            </div>
            <div class="col-md-1">
                @if (isset($model->image) && $model->image != null)
                <img src="{{ asset($model->image) }}" class="img-thumbnail image-preview" style="width: 100%; height: 100%;">
                @else
                <img src="{{ asset('assets/images/placeholder_image.png') }}" class="img-thumbnail image-preview" style="width: 100%; height: 100%;">
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-2"> صورة الهوية </label>
            <div class="col-md-9">
                <input type="file" name="identity_image" class="file-styled-primary identity-image">
            </div>
            <div class="col-md-1">
                @if (isset($model->identity_image) && $model->identity_image != null)
                    <img src="{{ asset($model->identity_image) }}" class="img-thumbnail image-preview" style="width: 100%; height: 100%;">
                @else
                    <img src="{{ asset('assets/images/placeholder_image.png') }}" class="img-thumbnail image-preview" style="width: 100%; height: 100%;">
                @endif
            </div>
        </div>


    <div class="form-group">
                <label class="control-label col-lg-2">تفعيل</label>
            <div class="col-md-10">
                {!! Form::select('active',[1=>"مفعل",0=>"غير مفعل"], null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>"إختر حالة التفعيل "]) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary pull-right">{{ $btnSub }}
                    <i class="{{ app()->getLocale() == 'ar' ? 'icon-arrow-left13' : 'icon-arrow-right13'}} position-right"></i></button>
            </div>
        </div>
</fieldset>


    @php
        $parent['route']=route('employee');
        $parent['name']="الموظفين";
      @endphp

    @push('parent')
    <li><a href="{{ $parent['route'] }}"><i class="icon-price-tags position-left"></i> {{ $parent['name'] }}</a></li>
    @endpush
    @section('current',$current)
