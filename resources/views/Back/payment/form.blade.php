<fieldset class="content-group">

    <div class="form-group">
        <label class="control-label col-lg-2">إسم المدفوع </label>
        <div class="col-lg-10">
            {!! Form::text("name", isset($model) ? $model->name:null , ['class'=>'form-control','placeholder'=>'إسم المدفوع']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2">ملاحظات  </label>
        <div class="col-md-10">
            {!! Form::text("desc", isset($model) ? $model->desc:null  , ['class'=>'form-control','placeholder'=>" ملاحظات "]) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2"> الإجمالي </label>
        <div class="col-lg-10">
            {!! Form::text("total", isset($model) ? $model->total:null , ['class'=>'form-control','placeholder'=>"الإجمالي "]) !!}
        </div>
    </div>


    <div class="form-group">
        <label class="control-label col-lg-2"> إختر القسم </label>
        <div class="col-md-10">
            {!! Form::select('category_id',$categories, null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>" إختر القسم ",'id'=>'payment-category']) !!}
        </div>
    </div>

    <div class="form-group" id="employee">
        <label class="control-label col-lg-2"> إختر الموظف </label>
        <div class="col-md-10">
            {!! Form::select('employee_id',$employees, null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>" إختر الموظف ",'id'=>'employee_id']) !!}
        </div>
    </div>



    @isset($model)
       @if($model->category_id != null )
       @if($model->category_id == 1)
       @if($model->employee_id != null )
        <div class="form-group" >
            <label class="control-label col-lg-2"> إختر الموظف </label>
            <div class="col-md-10">
                {!! Form::select('employee_id',$employees, null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>" إختر الموظف ",'id'=>'employee_id']) !!}
            </div>
        </div>
       @endif
       @endif
      @endif
    @endisset


    <div class="form-group">
        <label class="control-label col-lg-2">الصورة</label>
        <div class="col-md-9">
            <input type="file" name="image" class="file-styled-primary image">
        </div>
        <div class="col-md-1">
            @if (isset($model->image) && $model->image != null)
                <img src="{{ asset($model->image) }}" class="img-thumbnail image-preview" style="width: 100%; height: 100%;">
            @else
                <img src="{{ asset('assets/images/placeholder_image.png') }}" class="img-thumbnail image-preview" style="width: 100%; height: 100%;">
            @endif
        </div>
    </div>



    <div class="form-group">
        <label class="control-label col-lg-2">تفعيل</label>
        <div class="col-md-10">
            {!! Form::select('active',[1=>"مفعل",0=>"غير مفعل"], null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>"إختر حالة التفعيل "]) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary pull-right">{{ $btnSub }}
                <i class="{{ app()->getLocale() == 'ar' ? 'icon-arrow-left13' : 'icon-arrow-right13'}} position-right"></i></button>
        </div>
    </div>
</fieldset>


@php
        $parent['route']=route('payment');
        $parent['name'] = "المدفوعاات";
      @endphp

    @push('parent')
    <li><a href="{{ $parent['route'] }}"><i class="icon-price-tags position-left"></i> {{ $parent['name'] }}</a></li>
    @endpush
    @section('current',$current)

<script>
    $(document).ready(function () {
       $('#employee').hide();
       $('#payment-category').on('change',function (e) {
         if ($(this).val() == '1'){
            $('#employee').show();
         }else{
            $('#employee').hide();
            $('#employee_id').val('');
         }
       });
    });
</script>
