<!DOCTYPE html>
<html lang="en" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EDU</title>

    <!-- Global stylesheets -->

    <link href="{{ asset('assets/global/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/'.LaravelLocalization::getCurrentLocaleDirection().'/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/'.LaravelLocalization::getCurrentLocaleDirection().'/css/core.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/'.LaravelLocalization::getCurrentLocaleDirection().'/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/'.LaravelLocalization::getCurrentLocaleDirection().'/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/'.LaravelLocalization::getCurrentLocaleDirection().'/css/custom.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
    <!-- /global stylesheets -->
    <style>
        html * {
        /*font-family: 'Amiri', sans-serif;*/
        font-family: 'Cairo', sans-serif;
        font-weight: normal;
        font-style: normal;
        font-size: 14px;
        /* src: url("http://fonts.googleapis.com/earlyaccess/amiri.css"); */
        }
        .sidebar-user-material .category-content {
        background: url("{{ asset('assets/global/images/backgrounds/user_material_bg.jpg') }}") center center no-repeat;
        background-repeat: no-repeat;
        background-position: center center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        }
        .sidebar-user-material .category-content img{
            width: 85px;
            height: 85px;
            margin: auto;
            box-shadow: 0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);
        }


    </style>
    @yield('style')
    <!-- Core JS files -->
    <script src="{{ asset('assets/global/js/plugins/loaders/pace.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/core/libraries/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/core/libraries/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{ asset('assets/global/js/plugins/visualization/d3/d3.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
    <script src="{{ asset('assets/global/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script src="{{ asset('assets/global/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/plugins/pickers/daterangepicker.js') }}"></script>

    <script src="{{ asset('assets/global/js/plugins/ui/prism.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
   <script src="{{ asset('assets/global/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
    <script src="{{ asset('assets/global/js/demo_pages/user_pages_profile.js') }}"></script>
    <script src="{{ asset('assets/global/js/demo_pages/form_bootstrap_select.js') }}"></script>
    <script src="{{ asset('assets/global/js/demo_pages/form_inputs.js') }}"></script>
    <script src="{{ asset('assets/global/js/demo_pages/dashboard.js') }}"></script>
    <script src="{{ asset('assets/global/js/demo_pages/layout_fixed_native.js') }}"></script>
    <script src="{{ asset('assets/global/js/demo_pages/form_checkboxes_radios.js') }}"></script>
    <script src="{{ asset('assets/global/js/demo_pages/components_media.js') }}"></script>

    <script src="{{ asset('assets/global/js/plugins/ui/ripple.min.js') }}"></script>

    <script src="{{ asset('assets/global/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/plugins/notifications/noty.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/demo_pages/components_notifications_other.js') }}"></script>
    <!-- /theme JS files -->


</head>
<body class="navbar-top">
    @include('Back.includes.navbar')
    <!-- Page container -->
    @include('Back.includes.alert')
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">
            @include('Back.includes.sidebar')
            <div class="content-wrapper">
            @include('Back.includes.header')
                <div class="content">
                @yield('content')
                @include('Back.includes.footer')
              </div>
            </div>
        </div>
    </div>
        @yield('script')
        <script type="text/javascript" src="//w.24timezones.com/l.js" async></script>
        <script>
        $('.image').change(function(){
          if (this.files && this.files[0]) {
            var reader=new FileReader();
            reader.onload=function(e){
              $('.image-preview').attr('src',e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
          }
        });

        $('.avatar').change(function(){
          if (this.files && this.files[0]) {
            var reader=new FileReader();
            reader.onload=function(e){
              $('.avatar-preview').attr('src',e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
          }
        });
        </script>
        @yield('notify')
    </body>
</html>
