@extends('Back.layout')
@section('content')
<!-- Basic datatable -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">@lang('site.add')</h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
				<li><a data-action="reload"></a></li>
			</ul>
		</div>
	</div>
	<hr>
	<div class='container-fluid'>
		<div class="panel">
			<div class="panel-body">
				{!! Form::open(['route'=>'paymentcategory.store','method'=>'POST','files'=>true,'class'=>'form-horizontal']) !!}
				@include('Back.paymentcategory.form', ['btnSub' => trans('site.save'),'current'=>trans('site.add')])
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
<!-- /form horizontal -->
@endsection
