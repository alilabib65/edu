@extends('Back.layout')
@section('current',trans('site.admins'))
@section('content')
  <!-- Basic datatable -->
  <div class="panel panel-flat">
    <div class="panel-heading">
      <h5 class="panel-title"> المديرين </h5>
      <div class="heading-elements">
        <ul class="icons-list">
                  <li><a href="{{ route('admin.create') }}"><i class="icon-add"></i></a></li>
                  <li><a data-action="collapse"></a></li>
                  <li><a data-action="reload"></a></li>
                </ul>
              </div>
    </div>
    <hr>
  <div class='container-fluid'>
  <div class="panel">
  <div class="panel-body">
    <table class="table datatable-basic">
      <thead>
        <tr>
          <th>#</th>
          <th>@lang('site.image')</th>
          <th>@lang('site.name')</th>

          <th>@lang('site.show')</th>
          <th>@lang('site.added_date')</th>
          <th class="text-center">@lang('site.control')</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($model as $data)
        <tr class="{{ $data->id }}">
            <td>{{ $data->id }}</td>
            <td><img src="{{ asset($data->image) }}" class="img-rounded" style="width:100px; height:70px;"></td>
            <td>{{ $data->translate()->name }}</td>

            <td>
              <a href="" data-id="{{ $data->id }}" class="brand-active btn {{ $data->show ? 'btn-danger' : 'btn-info' }}">{{ $data->show ? trans('site.hide'):trans('site.show') }}</a></td>
            <td>{{ $data->created_at != null ? $data->created_at->format('d / m / Y') : '' }} </td>
            <td class="text-center">
              <ul class="icons-list">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-menu9"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{ route('admin.edit',$data->id) }}"><i class="icon-pencil"></i> @lang('site.edit') </a></li>

                    <li><a href="{{ route('admin.delete',$data->id) }}"  data-id={{ $data->id }}><i class="icon-database-remove"></i> @lang('site.delete') </a></li>


                  </ul>
                </li>
              </ul>
            </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>
  </div>
  <!-- /basic datatable -->

{{--
@include('dashboard.brand.modal') --}}

@endsection

@section('script')
 <script  src="{{ asset('assets/global/js/plugins/tables/datatables/datatables.min.js') }}"></script>
  <script  src="{{ asset('assets/global/js/plugins/forms/selects/select2.min.js') }}"></script>
  <script  src="{{ asset('assets/global/js/demo_pages/datatables_basic.js') }}"></script>
  <script>
      $('.datatable-basic').DataTable({
        "columnDefs": [ {
                "targets": [ 0 ]
            }],
        "language": {
            @if(app()->getLocale() == 'ar')
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
            @else
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/English.json"
            @endif
        }
      });

  {{-- $('.deleteBtn').on('click',function(e){
    e.preventDefault();
    var dataId=$(this).attr('data-id');
    $('.modalBtn').on('click',function(e){
      $("#modal_default").modal('hide')
      $.ajax({
        url: "{!! url('/') !!}/dashboard/admin/"+dataId,
        method: "DELETE",
        dataType:"json",
        data:{id:dataId,_token:"{{ csrf_token() }}"},
        success:function(data){
          if (data['value'] == 1) {
            $("."+brandId).fadeOut('slow',function(){
              $(this).remove();
            });
          }
        },
        error:function(err){
                $('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
                $.jGrowl("403", {
                    position: 'top-right',
                    theme: 'bg-warning',
                    header: 'ليس لديك صلاحيات الدخول ',
                    life: 3000

                });
        }
      });
    });
  }); --}}

  $('.brand-active').on('click',function(e){
     e.preventDefault();
     var dataId=$(this).attr('data-id');
     var status='state';
     var btn=$(this);
       $.ajax({
         url: "{!! url('/') !!}/Back/admin/"+dataId,
         method: "PUT",
         dataType:"json",
         data:{id:dataId,status:status,_token:"{{ csrf_token() }}"},
         success:function(data){
           if (data['value'] == 1) {
             btn.text(data['content']);
             if (data['class'] == 'btn-info') {
               btn.removeClass("btn-danger");
               btn.addClass("btn-info");
             }else{
               btn.removeClass("btn-info");
               btn.addClass("btn-danger");
             }

           }
         }
       });
   });

  </script>
@endsection
