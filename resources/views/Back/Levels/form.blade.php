<fieldset class="content-group">
        <ul class="nav nav-tabs text-center">
            <li class="active col-md-4"><a data-toggle="tab" href="#arabic">البيانات باللغة العربية</a></li>
            <li class="col-md-4"><a data-toggle="tab" href="#english">البيانات باللغة الإنجليزيه</a></li>
            <li class="col-md-4"><a data-toggle="tab" href="#public">بيانات عامه</a></li>
        </ul>

        <div class="tab-content">
        <div id="arabic" class="tab-pane fade in active">
        <div class="form-group">
            <label class="control-label col-lg-2">الإسم بالعربي</label>
            <div class="col-lg-10">
                {!! Form::text("ar[name]", isset($model) ? $model->translate('ar')->name:null , ['class'=>'form-control','placeholder'=>'الإسم بالعربي']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-2">الوصف بالعربي</label>
            <div class="col-md-10">
                {!! Form::textarea("ar[desc]", isset($model) ? $model->translate('ar')->desc:null  , ['class'=>'form-control','placeholder'=>"الوصف بالعربي"]) !!}
            </div>
        </div>
    </div>
    <div id="english" class="tab-pane fade">
        <div class="form-group">
            <label class="control-label col-lg-2">الإسم بالإنجليزية</label>
            <div class="col-lg-10">
                {!! Form::text("en[name]", isset($model) ? $model->translate('en')->name:null , ['class'=>'form-control','placeholder'=>"الإسم بالإنجليزية"]) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-2">الوصف بالإنجليزية</label>
            <div class="col-md-10">
                {!! Form::textarea("en[desc]", isset($model) ? $model->translate('en')->desc:null  , ['class'=>'form-control','placeholder'=>"الوصف بالإنجليزية"]) !!}
            </div>
        </div>
    </div>
        <div id="public" class="tab-pane fade">
        <div class="form-group">
            <label class="control-label col-lg-2">الصورة</label>
            <div class="col-md-9">
                <input type="file" name="image" class="file-styled-primary image">
            </div>
            <div class="col-md-1">
                @if (isset($model->image) && $model->image != null)
                <img src="{{ asset($model->image) }}" class="img-thumbnail image-preview" style="width: 100%; height: 100%;">
                @else
                <img src="{{ asset('assets/images/placeholder_image.png') }}" class="img-thumbnail image-preview" style="width: 100%; height: 100%;">
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-lg-2">المصاريف بدون تنقل</label>
            <div class="col-lg-4">
                {!! Form::text("no_bus_price", isset($model) ? $model->no_bus_price:null , ['class'=>'form-control','placeholder'=>'المصاريف بدون تنقل']) !!}
            </div>

            <label class="control-label col-lg-2">المصاريف  بالتنقل</label>
            <div class="col-lg-4">
                {!! Form::text("bus_price", isset($model) ? $model->bus_price:null , ['class'=>'form-control','placeholder'=>' المصاريف  بالتنقل']) !!}
            </div>
        </div>



            <div class="form-group">
                <label class="control-label col-lg-2">تفعيل</label>
            <div class="col-md-10">
                {!! Form::select('show',[1=>"مفعل",0=>"غير مفعل"], null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>"تفعيل"]) !!}
            </div>
        </div>
    </div>
    </div>
        <div class="form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary pull-right">{{ $btnSub }}
                    <i class="{{ app()->getLocale() == 'ar' ? 'icon-arrow-left13' : 'icon-arrow-right13'}} position-right"></i></button>
            </div>
        </div>
    </fieldset>


    @php
        $parent['route']=route('level');
        $parent['name']="المستويات الدراسيه";
      @endphp

    @push('parent')
    <li><a href="{{ $parent['route'] }}"><i class="icon-price-tags position-left"></i> {{ $parent['name'] }}</a></li>
    @endpush
    @section('current',$current)
