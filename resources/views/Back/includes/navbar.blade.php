<!-- Main navbar -->
<div class="navbar navbar-inverse navbar-fixed-top bg-indigo">
    <div class="navbar-header">
        <a class="navbar-brand" href=""><img src="" alt=""></a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>
    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                @if ($localeCode == LaravelLocalization::getCurrentLocale())
                    @continue
                @endif
                    <li>
                        <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            {{ $properties['native'] }}
                        </a>
                    </li>
            @endforeach
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-search4"></i>
                    <span class="visible-xs-inline-block position-right">البحث</span>
                </a>
                <div class="dropdown-menu dropdown-content width-350">
                    <div class="dropdown-content-body">
                        <div class="row">
                            <div class="form-group">
                                {!! Form::open(['url'=>'/search','method'=>'get']) !!}
                                <div class="col-md-10">
                                    <input type="text" name="search" placeholder="البحث" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info"><i class="icon-zoomin3"></i></button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="dropdown seen" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-bell2"></i>
                    <span class="visible-xs-inline-block position-right">الاشعارات</span>
                    <span class="badge bg-warning-400 count">{{-- $unseenOrder->count() --}}</span>
                </a>
                <div class="dropdown-menu dropdown-content width-350">
                    <div class="dropdown-content-heading">
                        الاشعارات
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-compose"></i></a></li>
                        </ul>
                    </div>
                    <ul class="media-list dropdown-content-body notify">
                        {{-- @foreach($unseenOrder as $order)
                        <li class="media">
                            <div class="media-left">
                                <img src="{{ asset($order->entity->logo) }}" class="img-circle img-sm" alt="">
                            </div>
                            <div class="media-body">
                                <a href="{{ route('order.show',$order->id) }}" class="media-heading">
                                    <span class="text-semibold">{{ $order->user->username }}</span>
                                    <span class="media-annotation pull-right">{{ $order->created_at ? $order->created_at->format('Y/m/d'): '' }}</span>
                                </a>
                                <span class="text-muted">{{ $order->total_price }}</span>
                            </div>
                        </li>
                        @endforeach --}}
                    </ul>
                    <div class="dropdown-content-footer">
                        {{-- <a href="{{ route('order.index') }}" data-popup="tooltip" title="عرض الكل"><i class="icon-menu display-block"></i></a> --}}
                    </div>
                </div>
            </li>
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ asset(Auth::user()->profile_image) }}" alt="{{ Auth::user()->username }}" style="width: 40px; height: 90px;">
                    <span>{{ Auth::user()->username }}</span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href=""><i class="icon-profile"></i> @lang('site.profile')</a></li>
                    <li><a href="{{ route('admin.logout') }}"><i class="icon-switch2"></i>@lang('site.logout')</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->
