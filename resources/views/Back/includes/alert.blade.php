<div class="col-sm-12">
    @if (session()->has("true"))
    @section('notify')
      <script type="text/javascript">
        $('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
        $.jGrowl("{{ session()->get("true") }}", {
            position: 'top-left',
            theme: 'bg-success',
            header: "{{ trans('site.done') }}",
            life: 6000
        });

      </script>
    @endsection
    @endif
    @if (session()->has("info"))
    @section('notify')
      <script type="text/javascript">
        $('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
        $.jGrowl("{{ session()->get("info") }}", {
            position: 'top-left',
            theme: 'bg-info',
            header: "{{ trans('site.info') }}",
            life: 6000
        });

      </script>
    @endsection
    @endif
    @if(count($errors) > 0)
    @section('notify')
      <script type="text/javascript">
        $('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
        @foreach($errors->all() as $error)
          $.jGrowl("{{ $error }}", {
              position: 'top-left',
              theme: 'bg-warning',
              header: "{{ trans('site.error') }}",
              life : 6000
            });
        @endforeach

      </script>
    @endsection

    @endif
    @if (session()->has("false"))
      @section('notify')
      <script type="text/javascript">
        $('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
        $.jGrowl("{{ session()->get("false") }}", {
            position: 'top-left',
            theme: 'bg-warning',
            header: "{{ trans('site.error') }}",
            life: 6000
        });

      </script>
    @endsection
    @endif
  </div>
