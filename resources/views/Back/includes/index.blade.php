@extends('Back.layout')
@section('content')
<!-- Basic datatable -->
<div class="panel panel-primary panel-bordered">
    <div class="panel-heading">
        <h5 class="panel-title"> @lang('site.public_stats') </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-3">
                <div class="panel bg-blue-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href=""><i class="icon-display"></i>@lang('site.show')</a></li>
                                        <li><a href=""><i class="icon-add"></i>@lang('site.add')</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <h3 class="no-margin"> {{ getModelCount('income') }} </h3>
                            @lang('site.income')
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel bg-pink-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href=""><i class="icon-display"></i>@lang('site.show')</a></li>
                                        <li><a href=""><i class="icon-add"></i>@lang('site.add')</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <h3 class="no-margin">{{getModelCount('payment')}}</h3>
                        @lang('site.outcome')
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel bg-teal-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href=""><i class="icon-display"></i>@lang('site.show')</a></li>
                                        <li><a href=""><i class="icon-add"></i>@lang('site.add')</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <h3 class="no-margin">{{getModelCount('student')}}</h3>
                        @lang('site.students')
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel bg-success-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href=""><i class="icon-display"></i>@lang('site.show')</a></li>
                                        <li><a href=""><i class="icon-add"></i>@lang('site.add')</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <h3 class="no-margin"> {{getModelCount('employee')}} </h3>
                        @lang('site.employee')
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="panel bg-brown-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href=""><i class="icon-display"></i>@lang('site.show')</a></li>
                                        <li><a href=""><i class="icon-add"></i>@lang('site.add')</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <h3 class="no-margin"> {{ getModelCount('level') }} </h3>
                        @lang('site.level')
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel bg-danger-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href=""><i class="icon-display"></i>@lang('site.show')</a></li>
                                        <li><a href=""><i class="icon-add"></i>@lang('site.add')</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <h3 class="no-margin">{{ getModelCount('paymentCategory') }} </h3>
                        @lang('site.payment_category')
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel bg-purple-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href=""><i class="icon-display"></i>@lang('site.show')</a></li>
                                        <li><a href=""><i class="icon-add"></i>@lang('site.add')</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <h3 class="no-margin">{{ getModelCount('employeeCategory') }}</h3>
                        @lang('site.employee_category')
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel bg-warning-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href=""><i class="icon-display"></i>@lang('site.show')</a></li>
                                        <li><a href=""><i class="icon-add"></i>@lang('site.add')</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <h3 class="no-margin"> {{ getModelCount('admin') }} </h3>
                         @lang('site.admin')
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="panel panel-success panel-bordered">
    <div class="panel-heading">
        <h5 class="panel-title"> @lang('site.main_accounts') </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-6">
                <div class="panel bg-blue-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href=""><i class="icon-display"></i>@lang('site.show')</a></li>
                                        <li><a href=""><i class="icon-add"></i>@lang('site.add')</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <h3 class="no-margin"> 0 </h3>
                        @lang('site.main_bank_account')
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="panel bg-pink-400">
                    <div class="panel-body">
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href=""><i class="icon-display"></i>@lang('site.show')</a></li>
                                        <li><a href=""><i class="icon-add"></i>@lang('site.add')</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <h3 class="no-margin">0</h3>
                        @lang('site.sub_payment_account')
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

@endsection
