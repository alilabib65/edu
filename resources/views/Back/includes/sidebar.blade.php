<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-default">
  <div class="sidebar-content">
    <!-- User menu -->
    <div class="sidebar-user-material">
      <div class="category-content">
        <div class="sidebar-user-material-content">
          <a href="#"><img src="{{ asset(Auth::user()->profile_image) }}" class="img-circle img-responsive" alt=""></a>
          <h6>{{ Auth::user()->fullname }}</h6>
          <span class="text-size-small">

          </span>
        </div>

        <div class="sidebar-user-material-menu">
          <a href="#user-nav" data-toggle="collapse"><span>الحساب</span> <i class="caret"></i></a>
        </div>
      </div>

      <div class="navigation-wrapper collapse" id="user-nav">
        <ul class="navigation">
          <li><a href=""><i class="icon-user-plus"></i> <span>الصفحه الشخصيه</span></a></li>
          <li><a href=""><i class="icon-gear"></i> <span>الإعدادات</span></a></li>
          <li><a href="{{ route('admin.logout') }}"><i class="icon-switch2"></i> <span>تسجيل الخروج</span></a></li>
        </ul>
      </div>
    </div>
        <!-- /user menu -->
        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <!-- Main -->
                    <li class="navigation-header"><span>@lang('site.menu') </span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="{{ request()->is($locale.'/dashboard/dashboard') ? 'active' : '' }}"><a href="{{route('admin-panel')}}"><i class="icon-home4"></i> <span>الرئيسية </span></a></li>



                    <li class="{{ request()->is($locale.'/dashboard/member/*') ||request()->is($locale.'/dashboard/member') ? 'active' : '' }}">
                        <a href="#"><i class="icon-users"></i> <span> @lang('site.employee')</span></a>
                        <ul>

                            <li><a href="{{route('employee')}}">@lang('site.show_all')</a></li>
                            <li><a href="{{route('employee.create')}}">@lang('site.add_new')  </a></li>
                            <li class="{{ request()->is($locale.'/dashboard/member/*') ||request()->is($locale.'/dashboard/member') ? 'active' : '' }}">
                                <a href="#"><i class="icon-users"></i> <span> @lang('site.employee_category')</span></a>
                                <ul>
                                    <li><a href="{{route('employeecategory')}}">@lang('site.show_all')</a></li>
                                    <li><a href="{{route('employeecategory.create')}}">@lang('site.add_new')  </a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="{{ request()->is($locale.'/dashboard/member/*') ||request()->is($locale.'/dashboard/member') ? 'active' : '' }}">
                        <a href="#"><i class="icon-users"></i> <span> @lang('site.income')</span></a>
                        <ul>
                            <li><a href="{{route('income')}}">@lang('site.show_all')</a></li>
                            <li><a href="{{route('income.create')}}">@lang('site.add_new')  </a></li>
                        </ul>
                    </li>

                    <li class="{{ request()->is($locale.'/dashboard/member/*') ||request()->is($locale.'/dashboard/member') ? 'active' : '' }}">
                        <a href="#"><i class="icon-users"></i> <span> @lang('site.payment')</span></a>
                        <ul>
                            <li><a href="{{route('payment')}}">@lang('site.show_all')</a></li>
                            <li><a href="{{route('payment.create')}}">@lang('site.add_new')  </a></li>
                            <li class="{{ request()->is($locale.'/dashboard/member/*') ||request()->is($locale.'/dashboard/member') ? 'active' : '' }}">
                                <a href="#"><i class="icon-users"></i> <span> @lang('site.payment_category')</span></a>
                                <ul>
                                    <li><a href="{{route('paymentcategory')}}">@lang('site.show_all')</a></li>
                                    <li><a href="{{route('paymentcategory.create')}}">@lang('site.add_new')  </a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="{{ request()->is($locale.'/dashboard/member/*') ||request()->is($locale.'/dashboard/member') ? 'active' : '' }}">
                        <a href="#"><i class="icon-users"></i> <span> @lang('site.student')</span></a>
                        <ul>
                            <li><a href="{{route('student')}}">@lang('site.show_all')</a></li>
                            <li><a href="{{route('student.create')}}">@lang('site.add_new')  </a></li>
                            <li class="{{ request()->is($locale.'/dashboard/member/*') ||request()->is($locale.'/dashboard/member') ? 'active' : '' }}">
                                <a href="#"><i class="icon-users"></i> <span> @lang('site.level')</span></a>
                                <ul>
                                    <li><a href="{{route('level')}}">@lang('site.show_all')</a></li>
                                    <li><a href="{{route('level.create')}}">@lang('site.add_new')  </a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <!-- /main -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>
