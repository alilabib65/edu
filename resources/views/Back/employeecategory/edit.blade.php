@extends('Back.layout')
@section('content')
<!-- Basic datatable -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">@lang('site.edit')</h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
				<li><a data-action="reload"></a></li>
			</ul>
		</div>
	</div>
    <hr>

	<div class='container-fluid'>
		<div class="panel">
			<div class="panel-body">
				{!! Form::model($model,['route'=>['category.update',$model->id],'method'=>'PUT','files'=>true,'class'=>'form-horizontal']) !!}
				@include('Back.Categories.form', ['btnSub' => trans('site.edit'),'current'=>trans('site.edit')])
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
<!-- /form horizontal -->
@endsection
