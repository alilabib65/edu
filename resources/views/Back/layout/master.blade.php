<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ direction() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="http-root" content="{{ url(Request::root()) }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script> window.laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!} </script>

    <title>@lang('site.dashboard') || @yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('admin/assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('admin/assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('admin/assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/js/pages/datatables_basic.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/js/pages/form_checkboxes_radios.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/assets/js/pages/components_modals.js') }}"></script>

    <script type="text/javascript" src="{{ asset('admin/assets/js/plugins/ui/ripple.min.js') }}"></script>
    <!-- /theme JS files -->

    @yield('style')
    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
</head>
<body class="@yield('bodyclass')">

    <div id="app">

        @if (!Request::is(app()->getLocale().'/admin-panel/login'))

        <div class="navbar navbar-inverse bg-indigo">
            <div class="navbar-header navbar-{{ app()->isLocale('ar') ? 'left' : 'right' }}">
                <a class="navbar-brand" style="float: {{ app()->isLocale('ar') ? 'right' : 'left' }};" href="{{ localeUrl('/admin-panel') }}">
                    <img src="{{ asset('admin/assets/images/logo_light.png') }}" alt="">
                </a>

                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav" style="float: {{ app()->isLocale('ar') ? 'right' : 'left' }};">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>

                <div class="navbar-{{ app()->isLocale('ar') ? 'right' : 'left' }}">
                    <ul class="nav navbar-nav" style="float: {{ app()->isLocale('ar') ? 'right' : 'left' }};">
                        <li class="dropdown language-switch">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                @php
                                    $lang = app()->isLocale('ar') ? 'sa' : 'gb';
                                @endphp
                                <img src="{{ asset('admin/assets/images/flags/'.$lang.'.png') }}" alt="">
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ LaravelLocalization::getLocalizedURL("en") }}" class="english">
                                        <img src="{{ asset('admin/assets/images/flags/gb.png') }}" alt=""> English
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ LaravelLocalization::getLocalizedURL("ar") }}" class="arabic">
                                        <img src="{{ asset('admin/assets/images/flags/sa.png') }}" alt=""> العربية
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    @auth('admin')
                        <p class="navbar-text">Morning, {{ $auth->name }} !</p>
                        <p class="navbar-text"><span class="label bg-success-400">@lang('backend.online')</span></p>
                    @endauth
                </div>
            </div>
        </div>

        @else

        <div class="navbar navbar-inverse bg-indigo">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ url('/admin-panel') }}">
                    <img src="{{ asset('admin/assets/images/logo_light.png') }}" alt="">
                </a>

                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                </ul>
            </div>
        </div>

        @endif

        <div class="page-container">

            <div class="page-content">
                @if (!Request::is(app()->getLocale().'/admin-panel/login'))

                <div class="sidebar sidebar-main sidebar-default">
                    <div class="sidebar-content">

                        <div class="sidebar-user-material">
                            <div class="category-content">
                                <div class="sidebar-user-material-content">
                                    <a href="javascript:void(0);">
                                        <img src="{{ getImage('admins', isset($auth) ? $auth->image : null) }}" class="img-circle img-responsive" alt="">
                                    </a>
                                    <h6>{{ ucwords(isset($auth) ? $auth->name : 'UNAUTH') }}</h6>
                                    <span class="text-size-small">{{ isset($auth) ? $auth->email : 'UNAUTH' }}</span>
                                </div>

                                <div class="sidebar-user-material-menu">
                                    <a href="#user-nav" data-toggle="collapse"><span>@lang('backend.my-account')</span> <i class="caret"></i></a>
                                </div>
                            </div>

                            <div class="navigation-wrapper collapse" id="user-nav">
                                <ul class="navigation">
                                    <li>
                                        <a href="">
                                            <i class="icon-user-plus"></i>
                                            <span>@lang('backend.my-account')</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form-1').submit();">
                                            <i class="icon-switch2"></i> @lang('backend.logout')
                                        </a>

                                        <form id="logout-form-1" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">

                                    <li class="navigation-header"><span>@lang('backend.main')</span>
                                        <i class="icon-menu" title="Main pages"></i>
                                    </li>

                                    <li class="{{ Request::is(app()->getLocale().'/admin-panel') ? 'active' : '' }}">
                                        <a href="{{ localeUrl('/admin-panel') }}">
                                            <i class="icon-home4"></i> <span>@lang('backend.dashboard')</span>
                                        </a>
                                    </li>

                                    @foreach (models() as $key => $model)

                                    @php
                                        $models = str()->plural($model);
                                    @endphp

                                    <li class="{{ Request::is(app()->getLocale().'/admin-panel/'.$models.'/*') ? 'active' : '' }}">
                                        <a href="#"><i class="icon-{{ $key }}"></i> <span>@lang('backend.'.$models)</span></a>
                                        <ul>
                                            <li class="{{ Request::is(app()->getLocale().'/admin-panel/'.$models.'/create') ? 'active' : '' }}">
                                                <a href="{{ localeUrl('/admin-panel/'.$models.'/create') }}">@lang('backend.add')</a>
                                            </li>
                                            <li class="{{ Request::is(app()->getLocale().'/admin-panel/'.$models) ? 'active' : '' }}">
                                                <a href="{{ localeUrl('/admin-panel/'.$models) }}">@lang('backend.all')</a>
                                            </li>
                                        </ul>
                                    </li>
                                    @endforeach

                                    <li class="{{ Request::is(app()->getLocale().'/admin-panel/reservations/*') ? 'active' : '' }}">
                                        <a href="#"><i class="icon-phone-incoming"></i> <span>@lang('backend.reservations')</span></a>
                                        <ul>
                                            <li class="{{ Request::is(app()->getLocale().'/admin-panel/reservations') ? 'active' : '' }}">
                                                <a href="{{ localeUrl('/admin-panel/reservations') }}">@lang('backend.all')</a>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                @endif

                <div class="content-wrapper">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <div id="site-modals"></div>

    <script type="text/javascript" src="{{ url('/assets/js/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('/assets/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('/assets/js/bootstrap-datetimepicker.min.js') }}" charset="UTF-8"></script>

    @yield('scripts')

    <script type="text/javascript" src="{{ asset('assets/js/crud.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/lang.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>
</body>
</html>
