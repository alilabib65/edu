<fieldset class="content-group">

    <div class="form-group">
        <label class="control-label col-lg-2">إسم الإيراد </label>
        <div class="col-lg-10">
            {!! Form::text("name", isset($model) ? $model->name:null , ['class'=>'form-control','placeholder'=>'إسم المدفوع']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2">ملاحظات  </label>
        <div class="col-md-10">
            {!! Form::text("desc", isset($model) ? $model->desc:null  , ['class'=>'form-control','placeholder'=>" ملاحظات "]) !!}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2"> الإجمالي </label>
        <div class="col-lg-10">
            {!! Form::text("total", isset($model) ? $model->total:null , ['class'=>'form-control','placeholder'=>"الإجمالي "]) !!}
        </div>
    </div>

    <div class="form-group" >
        <label class="control-label col-lg-2"> إختر الطالب </label>
        <div class="col-md-10">
            {!! Form::select('student_id',$students, null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>" إختر الطالب "]) !!}
        </div>
    </div>

    <div class="form-group" id="level_payment">

    </div>


    <div class="form-group">
        <label class="control-label col-lg-2"> إختر المرحلة </label>
        <div class="col-md-10">
            {!! Form::select('level_id',$levels, null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>" إختر المرحلة ",'id'=>'payment-category']) !!}
        </div>
    </div>





    <div class="form-group">
        <label class="control-label col-lg-2">الصورة</label>
        <div class="col-md-9">
            <input type="file" name="image" class="file-styled-primary image">
        </div>
        <div class="col-md-1">
            @if (isset($model->image) && $model->image != null)
                <img src="{{ asset($model->image) }}" class="img-thumbnail image-preview" style="width: 100%; height: 100%;">
            @else
                <img src="{{ asset('assets/images/placeholder_image.png') }}" class="img-thumbnail image-preview" style="width: 100%; height: 100%;">
            @endif
        </div>
    </div>



    <div class="form-group">
        <label class="control-label col-lg-2">تفعيل</label>
        <div class="col-md-10">
            {!! Form::select('active',[1=>"مفعل",0=>"غير مفعل"], null , ['class'=>'form-control bootstrap-select','data-live-search'=>"true",'placeholder'=>"إختر حالة التفعيل "]) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary pull-right">{{ $btnSub }}
                <i class="{{ app()->getLocale() == 'ar' ? 'icon-arrow-left13' : 'icon-arrow-right13'}} position-right"></i></button>
        </div>
    </div>
</fieldset>

@php
        $parent['route']=route('income');
        $parent['name']="الإيرادات ";
      @endphp

    @push('parent')
    <li><a href="{{ $parent['route'] }}"><i class="icon-price-tags position-left"></i> {{ $parent['name'] }}</a></li>
    @endpush
    @section('current',$current)


<script>
    $(document).ready(function () {
    });
</script>
