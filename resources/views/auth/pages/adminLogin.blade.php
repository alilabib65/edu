@extends('Back.layout.master')

@section('title', trans('site.login'))

@section('bodyclass', 'login-container')

@section('content')
	<!-- Content area -->
	<div class="content">

		<!-- Simple login form -->
		<form method="POST" action="{{ route('admin.submit.login') }}">
			@csrf
			<div class="panel panel-body login-form">
				<div class="text-center">
					<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
					<h5 class="content-group">@lang('site.login_to_your_account') <small class="display-block">@lang('site.enter_credentials')</small></h5>
				</div>

				<div class="form-group has-feedback has-feedback-left">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="@lang('site.email')" autocomplete="email" autofocus>

					<div class="form-control-feedback">
						<i class="icon-user text-muted"></i>
					</div>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

				<div class="form-group has-feedback has-feedback-left">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" placeholder="@lang('site.password')" autocomplete="password" autofocus>

					<div class="form-control-feedback">
						<i class="icon-lock2 text-muted"></i>
					</div>

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong style="color: red;">{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

				<div class="form-group">
					<button type="submit" class="btn bg-pink-400 btn-block">@lang('site.login') <i class="icon-circle-left2 position-right"></i></button>
				</div>

			</div>
		</form>
		<!-- /simple login form -->


		<!-- Footer -->
		<div class="footer text-muted text-center">
			&copy; {{ date('Y') }}. <a href="#">@lang('backend.footer')</a> by <a href="#" target="_blank"> Ali Labib</a>
		</div>
		<!-- /footer -->

	</div>
	<!-- /content area -->
@stop
