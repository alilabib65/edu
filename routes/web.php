<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

# User Login
//Route::group([
//	'prefix' => LaravelLocalization::setLocale(),
//	'namespace'  => 'Auth\User',
//	'middleware' => [
//		'localeSessionRedirect',
//		'localizationRedirect',
//		'localeViewPath'
//	]
//],
//function() {
//	Route::get('/login', 'UserLoginController@showUserLoginForm')->name('user.login');
//	Route::post('/login', 'UserLoginController@userLogin')->name('user.submit.login');
//	Route::post('/logout', 'UserLoginController@userLogout')->name('user.logout');
//});

# Admin Login
Route::group([
	'prefix' => LaravelLocalization::setLocale().'/admin-panel',
	'middleware' => [
		'localeSessionRedirect',
		'localizationRedirect',
        'localeViewPath',

	]
],
function() {

	Route::get('/login', 'Auth\Admin\AdminLoginController@showAdminLoginForm')->name('admin-panel.login');
	Route::post('/login', 'Auth\Admin\AdminLoginController@adminLogin')->name('admin.submit.login');
    Route::get('/layout' ,function (){
        return view('Back.layout.master1');
    });

    Route::group(['middleware' => 'auth:admin','namespace' => 'Back'], function () {
        Route::get('/logout', 'Auth\Admin\AdminLoginController@adminLogout')->name('admin.logout');

        Route::get('/','HomeController@index')->name('admin-panel');

        //Admins
        Route::get('admins','ActivitesController@index')->name('admin');
        Route::get('admins/create','ActivitesController@create')->name('admin.create');
        Route::post('admins/store','ActivitesController@store')->name('admin.store');
        Route::get('admins/edit/{model}','ActivitesController@edit')->name('admin.edit');
        Route::put('admins/update/{model}','ActivitesController@update')->name('admin.update');
        Route::get('admins/delete/{model}','ActivitesController@delete')->name('admin.delete');

        //EmployeeCategory
        Route::get('employeecategory','EmployeeCategoriesController@index')->name('employeecategory');
        Route::get('employeecategory/create','EmployeeCategoriesController@create')->name('employeecategory.create');
        Route::post('employeecategory/store','EmployeeCategoriesController@store')->name('employeecategory.store');
        Route::get('employeecategory/edit/{model}','EmployeeCategoriesController@edit')->name('employeecategory.edit');
        Route::put('employeecategory/update/{model}','EmployeeCategoriesController@update')->name('employeecategory.update');
        Route::get('employeecategory/delete/{model}','EmployeeCategoriesController@delete')->name('employeecategory.delete');


        //Employee
        Route::get('employee','EmployeesController@index')->name('employee');
        Route::get('employee/create','EmployeesController@create')->name('employee.create');
        Route::post('employee/store','EmployeesController@store')->name('employee.store');
        Route::get('employee/edit/{model}','EmployeesController@edit')->name('employee.edit');
        Route::put('employee/update/{model}','EmployeesController@update')->name('employee.update');
        Route::get('employee/delete/{model}','EmployeesController@delete')->name('employee.delete');

        //Income
        Route::get('income','IncomeController@index')->name('income');
        Route::get('income/create','IncomeController@create')->name('income.create');
        Route::post('income/store','IncomeController@store')->name('income.store');
        Route::get('income/edit/{model}','IncomeController@edit')->name('income.edit');
        Route::put('income/update/{model}','IncomeController@update')->name('income.update');
        Route::get('income/delete/{model}','IncomeController@delete')->name('income.delete');



        //Levels
        Route::get('level','LevelController@index')->name('level');
        Route::get('level/create','LevelController@create')->name('level.create');
        Route::post('level/store','LevelController@store')->name('level.store');
        Route::get('level/edit/{model}','LevelController@edit')->name('level.edit');
        Route::put('level/update/{model}','LevelController@update')->name('level.update');
        Route::get('level/delete/{model}','LevelController@delete')->name('level.delete');


        //Payment Categories
        Route::get('paymentcategory','PaymentCategoryController@index')->name('paymentcategory');
        Route::get('paymentcategory/create','PaymentCategoryController@create')->name('paymentcategory.create');
        Route::post('paymentcategory/store','PaymentCategoryController@store')->name('paymentcategory.store');
        Route::get('paymentcategory/edit/{model}','PaymentCategoryController@edit')->name('paymentcategory.edit');
        Route::put('paymentcategory/update/{model}','PaymentCategoryController@update')->name('paymentcategory.update');
        Route::get('paymentcategory/delete/{model}','PaymentCategoryController@delete')->name('paymentcategory.delete');


        //Payment
        Route::get('payment','PaymentController@index')->name('payment');
        Route::get('payment/create','PaymentController@create')->name('payment.create');
        Route::post('payment/store','PaymentController@store')->name('payment.store');
        Route::get('payment/edit/{model}','PaymentController@edit')->name('payment.edit');
        Route::put('payment/update/{model}','PaymentController@update')->name('payment.update');
        Route::get('payment/delete/{model}','PaymentController@delete')->name('payment.delete');



        //Students
        Route::get('student','StudentsController@index')->name('student');
        Route::get('student/create','StudentsController@create')->name('student.create');
        Route::post('student/store','StudentsController@store')->name('student.store');
        Route::get('student/edit/{model}','StudentsController@edit')->name('student.edit');
        Route::put('student/update/{model}','StudentsController@update')->name('student.update');
        Route::get('student/delete/{model}','StudentsController@delete')->name('student.delete');


    });

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
