// $('.deleteBtn').on('click', function(e) {
//     e.preventDefault();
//     var typeId = $(this).attr('type-id');
//     $('.modalBtn').on('click', function(e) {
//         $("#modal_default").modal('hide');
//         $.ajax({
//             url: "/dashboard/type/" + typeId,
//             method: "DELETE",
//             dataType: "json",
//             data: {
//                 type_id: typeId,
//                 _token: "{{ csrf_token() }}"
//             },
//             success: function(data) {
//                 if (data['value'] == 1) {
//                     $("." + typeId).fadeOut('slow', function() {
//                         $(this).remove();
//                     });
//                 }
//             },
//             error: function(err) {
//                 $('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
//                 $.jGrowl("403", {
//                     position: 'top-right',
//                     theme: 'bg-warning',
//                     header: 'ليس لديك صلاحيات الدخول ',
//                     life: 3000
//
//                 });
//             }
//         });
//     });
// });
// $('.update').submit(function(e) {
//   e.preventDefault();
//   var tId=$(this).find('input[name="id"]').val();
//   var name=$(this).find('input[name="name"]').val();
//   $.ajax({
//       url:  "{!! url('/') !!}/dashboard/type/"+ tId,
//       method: "PUT",
//       dataType: "json",
//       data: {
//            name : name,
//            id : tId,
//           _token: "{{ csrf_token() }}"
//       },
//       success: function(data) {
//           if (data['value'] == 1) {
//             $('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
//             $.jGrowl("200", {
//                 position: 'top-right',
//                 theme: 'bg-success',
//                 header: 'تم التعديل بنجاح',
//                 life: 3000
//
//             });
//           }
//       },
//       error: function(err) {
//           $('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
//           $.jGrowl("403", {
//               position: 'top-right',
//               theme: 'bg-warning',
//               header: 'ليس لديك صلاحيات الدخول ',
//               life: 3000
//
//           });
//       }
//   });
//
// });
// $('.store').submit(function(e) {
//   e.preventDefault();
//   var name=$('input[name="name"]').val();
//   $.ajax({
//       url: "{{ route('type.store') }}",
//       method: "POST",
//       dataType: "json",
//       data: {
//            name : name,
//           _token: "{{ csrf_token() }}"
//       },
//       success: function(data) {
//           if (data['value'] == 1) {
//             $(this).find('input[name="name"]').val('');
//               $("tbody").append(data['view']);
//           }
//       },
//       error: function(err) {
//           $('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
//           $.jGrowl("403", {
//               position: 'top-right',
//               theme: 'bg-warning',
//               header: 'ليس لديك صلاحيات الدخول ',
//               life: 3000
//
//           });
//       }
//   });
//
// });
